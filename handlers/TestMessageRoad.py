# -*- coding: UTF-8 -*-
from handlers.BaseHandler import BaseHandler
from common.send_itchat import *
from common.send_email import send_notify_email
from common import wulai_logger as logger
from test_wx_mini_backend import wx_mini_backend_interface

import os

def out_network_connected():
    import requests
    try:
        html = requests.get("http://www.baidu.com", timeout=2)
        print("baidu")
        print(html.text)
        # 返回响应头
        print(html.status_code)
    except:
        return False
    return True

def ping_openapi_wulai():
    url = 'https://mplugin.wul.ai/get_configure2?client=f8Hpkr5cJWOaegCLbQ6VD1ES8g9W8xL900cb60581e0ba93511'
    response = requests.get(url, timeout=5)
    # 返回信息
    logger.info(response.text)
    # 返回响应头
    print(response.status_code)
    if str(response.status_code) == "200":
        return True
    else:
        return False

def out_laiye_com():
    import requests
    try:
        html = requests.get("http://mplugin.laiye.com", timeout=2)
        print("laiye")
        print(html.text)
        # 返回响应头
        print(html.status_code)
    except:
        return False
    return True


class TestMessageRoad(BaseHandler):

    def get(self, *args, **kwargs):
        msg = {}
        code = wx_mini_backend_interface.test_callback()
        if str(code) == "200":
            msg["投递融云"] = "投递成功，状态码 : " + str(code)
        else:
            msg["投递融云"] = "投递失败，状态码 : " + str(code)

        if out_network_connected():
            msg["链接外网"] = "链接正常"
        else:
            msg["链接外网"] = "链接失败"

        if out_laiye_com():
            msg["链接代理层"] = "链接正常"
        else:
            msg["链接代理层"] = "链接失败"

        if wx_mini_backend_interface.test_callback():
            msg["回调融云接口"] = "回调正常"
        else:
            msg["回调融云接口"] = "回调失败"

        print(ping_openapi_wulai())
        if ping_openapi_wulai():
            msg["小程序到渠道后端到吾来"] = "链路正常"
        else:
            msg["小程序到渠道后端到吾来"] = "链路失败"
        send_notify_email("通知",str(msg))
        self.write(msg)
        self.set_header('Access-Control-Allow-Origin', '*')
        self.set_header('Content-Type', 'application/json; charset=UTF-8')
        self.finish()



    def post(self, *args, **kwargs):
        print('<<<<<<<<<<<<<<<<<<<<<< in SendAlarmHandler <<<<<<<<<<<<<<<<<<<<<<')
        params = json.loads(self.request.body)
        logger.error("cao--------------")
        # params = json.loads(str(self.request.body, encoding='utf-8'))
        senderUserId = params.get("senderUserId", '')
        targetId = params.get("targetId", '')
        messageUId = str(params.get("messageUId", ''))
        extra = params.get('extra', '')
        success = str(params.get('success', ''))
        ts = params.get('ts', '')

        try:
            logger.error("senderUserId: {} targetId:{}, messageUId: {}, extra:{}, success:{}, ts:{}".format(senderUserId, targetId, messageUId, extra, success, ts))
            msg ={
               "code":0,
               "msg":"send successfully"
            }
        except:
            msg ={
               "code":-1,
               "msg":"send failed"
            }

        self.write(msg)
        self.set_header('Access-Control-Allow-Origin', '*')
        self.set_header('Content-Type', 'application/json; charset=UTF-8')
        self.finish()

# -----------------------------------------------------------------------------
# URL to handler mappings
# -----------------------------------------------------------------------------
# path matches any number of `/foo[/bar...]` or just `/` or ''
path_regex = r"(?P<path>(?:(?:/[^/]+)+|/?))"

default_handlers = [
    (r"/v1/test%s" % path_regex, TestMessageRoad),
    (r"/v1/test/*.*", TestMessageRoad),
# (r'/pic/*.*', BmiPictureHandler)
]

# Sept-12-2018  QiChang.Yin         created.