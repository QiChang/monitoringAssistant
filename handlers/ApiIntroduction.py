# -*- coding: UTF-8 -*-
import json
import requests
from handlers.BaseHandler import BaseHandler

from common.send_itchat import *
from conf.constant import msg_key,msg_title
import sys,os
import urllib

class ApiIntroductionHandler(BaseHandler):
    def get(self, *args, **kwargs):

        file_name = self.request.uri.split('/')[-1:][0]
        self.set_header('Content-Type', 'application/octet-stream')
        self.set_header('Content-Disposition', 'attachment; filename=%s' % file_name)
        file_dir = os.path.join(os.path.dirname(os.path.dirname(__file__)), "static/upload/")
        path = os.path.join(file_dir, file_name)
        with open(path, 'rb') as f:
            while True:
                data = f.read(4096)
                if not data:
                    break
                self.write(data)
        self.finish()
    #
    # def get(self, *args, **kwargs):
    #     file_name = self.request.uri.split('/')[-1:][0]
    #     self.set_header('Content-Type', 'application/octet-stream')
    #     self.set_header('Content-Disposition', 'attachment; filename=%s' % file_name)
    #     file_dir = os.path.join(os.path.dirname(os.path.dirname(__file__)), "static/upload/")
    #     path = os.path.join(file_dir, file_name)
    #     with open(path, 'rb') as f:
    #         while True:
    #             data = f.read(4096)
    #             if not data:
    #                 break
    #             self.write(data)
    #     self.finish()
    #

    # def get(self, *args, **kwargs):
    #     path = os.path.join(os.path.dirname(os.path.dirname(__file__)), "static/upload/")
    #     print(path)
    #     log_name = path + self.request.uri.split('/')[-1:][0]
    #     fi = "qqqq.log"
    #     self.set_header('Content-Type', 'application/octet-stream')
    #     self.set_header('Content-Disposition', 'attachment; filename=%s' % fi)
    #     filedir = os.path.join(os.path.dirname(os.path.dirname(__file__)), "static/upload/")
    #     path = os.path.join(filedir, fi)
    #     with open(path, 'rb') as f:
    #         while True:
    #             data = f.read(4096)
    #             if not data:
    #                 break
    #             self.write(data)
    #     self.finish()


    def post(self, *args, **kwargs):
        logger.info('<<<<<<<<<<<<<<<<<<<<<< in SendAlarmHandler <<<<<<<<<<<<<<<<<<<<<<')
        params = json.loads(self.request.body)
        logger.error("cao--------------")
        self.write("hello world")
        self.set_header('Access-Control-Allow-Origin', '*')
        self.set_header('Content-Type', 'application/json; charset=UTF-8')
        self.finish()

# -----------------------------------------------------------------------------
# URL to handler mappings
# -----------------------------------------------------------------------------
# path matches any number of `/foo[/bar...]` or just `/` or ''
path_regex = r"(?P<path>(?:(?:/[^/]+)+|/?))"

default_handlers = [
    (r"/v1/api%s" % path_regex, ApiIntroductionHandler),
    (r"/v1/api/*.*", ApiIntroductionHandler),
]

# Sept-12-2018  QiChang.Yin         created.