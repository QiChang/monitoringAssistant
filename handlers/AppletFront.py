# -*- coding: UTF-8 -*-
import json
import requests
from handlers.BaseHandler import BaseHandler

from common.send_itchat import *
from common import wulai_logger as logger
from conf.constant import msg_key,msg_title
import sys,os
import  shutil
import urllib

class AppletFrontHandler(BaseHandler):
    # def get(self, *args, **kwargs):
    #
    #     path = os.path.join(os.path.dirname(os.path.dirname(__file__)), "static/upload/")
    #     print(path)
    #     log_name = path + self.request.uri.split('/')[-1:][0]
    #     print('pic_name:', log_name)
    #     with open(log_name, 'rb') as f:
    #         result = f.read()
    #         print(result)
    #         self.write(result)
    #         self.set_header('Access-Control-Allow-Origin', '*')
    #         self.set_header('Content-Type', 'application/octet-stream')
    #         self.set_header('Content-Type', 'text/html; charset=UTF-8')
    #
    def get(self, *args, **kwargs):

        sc_dir = os.path.join(os.path.dirname(os.path.dirname(__file__)), "logs")
        sc_file = sc_dir + "/info.log"
        de_dir= os.path.join(os.path.dirname(os.path.dirname(__file__)), "static/upload/")
        de_file = de_dir + "/info.log"
        shutil.copyfile(sc_file,de_file)
        file_name = self.request.uri.split('/')[-1:][0]
        self.set_header('Content-Type', 'application/octet-stream')
        self.set_header('Content-Disposition', 'attachment; filename=%s' % file_name)
        file_dir = os.path.join(os.path.dirname(os.path.dirname(__file__)), "static/upload/")
        path = os.path.join(file_dir, file_name)
        with open(path, 'rb') as f:
            while True:
                data = f.read(4096)
                if not data:
                    break
                self.write(data)
        self.finish()


    # def get(self, *args, **kwargs):
    #     path = os.path.join(os.path.dirname(os.path.dirname(__file__)), "static/upload/")
    #     print(path)
    #     log_name = path + self.request.uri.split('/')[-1:][0]
    #     fi = "qqqq.log"
    #     self.set_header('Content-Type', 'application/octet-stream')
    #     self.set_header('Content-Disposition', 'attachment; filename=%s' % fi)
    #     filedir = os.path.join(os.path.dirname(os.path.dirname(__file__)), "static/upload/")
    #     path = os.path.join(filedir, fi)
    #     with open(path, 'rb') as f:
    #         while True:
    #             data = f.read(4096)
    #             if not data:
    #                 break
    #             self.write(data)
    #     self.finish()


    def post(self, *args, **kwargs):
        print('<<<<<<<<<<<<<<<<<<<<<< in SendAlarmHandler <<<<<<<<<<<<<<<<<<<<<<')
        params = json.loads(self.request.body)
        logger.error("cao--------------")
        # params = json.loads(str(self.request.body, encoding='utf-8'))
        senderUserId = params.get("senderUserId", '')
        targetId = params.get("targetId", '')
        messageUId = str(params.get("messageUId", ''))
        extra = params.get('extra', '')
        success = str(params.get('success', ''))
        ts = params.get('ts', '')

        try:
            logger.error("senderUserId: {} targetId:{}, messageUId: {}, extra:{}, success:{}, ts:{}".format(senderUserId, targetId, messageUId, extra, success, ts))
            msg ={
               "code":0,
               "msg":"send successfully"
            }
        except:
            msg ={
               "code":-1,
               "msg":"send failed"
            }

        self.write(msg)
        self.set_header('Access-Control-Allow-Origin', '*')
        self.set_header('Content-Type', 'application/json; charset=UTF-8')
        self.finish()

# -----------------------------------------------------------------------------
# URL to handler mappings
# -----------------------------------------------------------------------------
# path matches any number of `/foo[/bar...]` or just `/` or ''
path_regex = r"(?P<path>(?:(?:/[^/]+)+|/?))"

default_handlers = [
    (r"/v1/applet%s" % path_regex, AppletFrontHandler),
    (r"/v1/applet/*.*", AppletFrontHandler),
# (r'/pic/*.*', BmiPictureHandler)
]

# Sept-12-2018  QiChang.Yin         created.