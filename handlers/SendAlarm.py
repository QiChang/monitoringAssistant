# -*- coding: UTF-8 -*-
import json
from handlers.BaseHandler import BaseHandler
# from common import wulai_reply
from common import wulai_logger as logger
from common.send_email import send_notify_email
from common.send_itchat import *
from conf.constant import msg_key,msg_title


class SendAlarmHandler(BaseHandler):

    def get(self, *args, **kwargs):
        self.write("It works")

    def post(self, *args, **kwargs):
        logger.info('<<<<<<<<<<<<<<<<<<<<<< in SendAlarmHandler <<<<<<<<<<<<<<<<<<<<<<')
        print(self.request.body)
        params = json.loads(self.request.body)
        logger.info('input params=%s' % json.dumps(params))
        msg = ""
        ret = ["", "", ""]
        if "target_type" in params and params["target_type"].isdigit() and int(params["target_type"]) < len(msg_title):
            msg = msg_title[int(params["target_type"])]+"\n"
        get_value = {v: params.get(k) for k, v in msg_key.items() if k in params}
        for k in sorted(get_value.keys(), reverse=True):
            msg = msg + (k + ":" + get_value[k] + '\n').format(get_value[k])
        logger.info(msg)
        if 'to_user_name' in params and len(params['to_user_name']) > 0:
            if send_to_one_person(msg=msg, pname=params['to_user_name']):
                ret[0] = "发送个人成功"
            else:
                ret[0] = "发送个人失败"
            print(params['to_user_name'])
        if 'to_group_name' in params and len(params['to_group_name']) > 0:
            if send_to_one_group(msg=msg, gname=params['to_group_name']):
                ret[1] = "发送给群成功"
            else:
                ret[1] = "发送给群失败"
            print(params['to_group_name'])
        if 'to_email' in params and len(params['to_email']) > 0:
            if send_notify_email(params['to_email_title'], msg):
                ret[2] = "发送邮件成功"
            else:
                ret[2] = "发送邮件失败"
            print(params['to_email'])

        self.write(str(ret))
        self.set_header('Access-Control-Allow-Origin', '*')
        self.set_header('Content-Type', 'application/json; charset=UTF-8')
        self.finish()

# -----------------------------------------------------------------------------
# URL to handler mappings
# -----------------------------------------------------------------------------
# path matches any number of `/foo[/bar...]` or just `/` or ''
path_regex = r"(?P<path>(?:(?:/[^/]+)+|/?))"

default_handlers = [
    (r"/v1/alarm%s" % path_regex, SendAlarmHandler),
    (r"/v1/alarm", SendAlarmHandler),
]

# Sept-12-2018  QiChang.Yin         created.