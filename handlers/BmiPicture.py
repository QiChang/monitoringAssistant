# -*- coding: UTF-8 -*-
import os
from handlers.BaseHandler import BaseHandler


class BmiPictureHandler(BaseHandler):

    def get(self):
        path = os.path.join(os.path.dirname(os.path.dirname(__file__)), "static/img/")
        pic_name = path + self.request.uri.split('/')[-1:][0]
        print('pic_name', pic_name)
        with open(pic_name, 'rb') as pic:
            pic_io = pic.read()
            self.write(pic_io)
            self.set_header("Content-type", "image/png")

# -----------------------------------------------------------------------------
# URL to handler mappings
# -----------------------------------------------------------------------------
# path matches any number of `/foo[/bar...]` or just `/` or ''
path_regex = r"(?P<path>(?:(?:/[^/]+)+|/?))"

default_handlers = [
    (r'/pic/*.*', BmiPictureHandler)
]

# Sept-12-2018  QiChang.Yin         created.