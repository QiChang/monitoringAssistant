#!/usr/bin/env bash
####################################################################################################
#
# Version: v1.0
# Date: 2018-09-12
# File: git_app.sh
# Author: QiChang.Yin
# Git version: git version 2.7.4
# Kernel version: 4.4.0-21-generic
# Operation system: Ubuntu version 16.04
# Instruction: The script is convenient for using control system.
#
###################################################################################################


#添加Controller的IP地址和端口号
CONTROLLER_IP_ADDRESS="139.219.128.71"
copy_path=$(pwd)
echo $copy_path

function local_to_server
{
    scp -r $copy_path  works@$CONTROLLER_IP_ADDRESS:/home/works/projects/liujinyan
}

function server_to_local
{
    scp -r $copy_path  liujinyan@192.168.10.47:/Users/liujinyan/Desktop
}


if [ $1 == "lts" ];
then
    local_to_server
elif [ "$1" == "stl" ];
then
    server_to_local
fi
