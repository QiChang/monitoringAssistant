# -*- coding:utf-8 -*-

from wxpy import *
from common.send_email import send_notify_email
itchat = Bot(console_qr=2, cache_path="itchat.pkl")


def send_to_one_person(**kwargs):

    """
    :param kwargs:
    :return:
    """
    print()
    message = str("【报警通知🚔🚔🚔】\n告警服务器:{host_name}\n"
                   "告警时间:{trigger_time}\n"
                   "告警IP:{host_ip}\n"
                   "告警项:{description}\n"
                   "告警级别:{level}\n"
                   "告警信息:{msg}\n"
                   "发送人:{to_user_name}\n"
                   "收件箱:{email}\n"
                   "详细信息:{detail}".format(
                    host_name=kwargs["host_name"],
                    trigger_time=kwargs["trigger_time"],
                    host_ip=kwargs["host_ip"],
                    description=kwargs["description"],
                    level=kwargs["level"],
                    msg=kwargs["msg"],
                    to_user_name=kwargs["to_user_name"],
                    email=kwargs['email'],
                    detail = kwargs['detail']
                    ))

    try:
        if kwargs['email'] is not None and len(kwargs['email']) > 0:
            send_notify_email("报警邮件", message, receiver=kwargs['email'])
        else:
            print("不发送邮件")
    except:
        print("发送邮件异常")

    try:
        itchat = Bot(console_qr=2, cache_path="itchat.pkl")
        user_name = kwargs["to_user_name"]
        friend = itchat.friends().search(name=user_name)
        print(message)
        for index, item in enumerate(friend):
            print("发送给 " + str(item) + " ing,index=" + str(index))
            item.send(message)
    except:
        error_message = "发送错误"
        for index, item in enumerate(friend):
            item.send(error_message)


if __name__ == "__main__":

    test = dict()
    test = {
        "host_name":"中央办公厅",
        "trigger_time":"2018-10-10 00:00:00",
        "host_ip":"111.111.111.111",
        "description":"中奖11111110万",
        "level":"紧急",
        "msg":"赶紧领取iiii",
        "to_user_name":"林深时见鹿",
        "email":"yinqichang@laiye.com",
        "detail": "http://jk-saasv2.laiye.com/job/Deploy-Stage-docker/675/",
    }
    send_to_one_person(**test)