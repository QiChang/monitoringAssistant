# -*- coding:utf-8 -*-
import json
import itchat
import traceback
from common import wulai_logger as logger
import requests
from wxpy import *

itchat.auto_login(hotReload=True)

# itchat.run()
# 调用图灵机器人的api，采用爬虫的原理，根据聊天消息返回回复内容
# def tuling(info):
#     print("tuling",info)
#     print(type(info))
#     appkey = "e5ccc9c7c8834ec3b08940e290ff1559"
#     url = "http://www.tuling123.com/openapi/api?key=%s&info=%s"%(appkey,info)
#     req = requests.get(url)
#     content = req.text
#     data = json.loads(content)
#     answer = data['text']
#     return answer
#     # else:
#     #     return None
#
#
# # 对于群聊信息，定义获取想要针对某个群进行机器人回复的群ID函数
# def group_id(name):
#     print(name)
#     df = itchat.search_chatrooms(name=name)
#     return df[0]['UserName']
#
#
# # 注册文本消息，绑定到text_reply处理函数
# # text_reply msg_files可以处理好友之间的聊天回复
# @itchat.msg_register([TEXT,MAP,CARD,NOTE,SHARING])
# def text_reply(msg):
#     print("text_reply",text_reply)
#     itchat.send('%s' % tuling(msg['Text']),msg['FromUserName'])
#
# @itchat.msg_register([PICTURE, RECORDING, ATTACHMENT, VIDEO])
# def download_files(msg):
#     print("download_files",msg)
#     msg['Text'](msg['FileName'])
#     return '@%s@%s' % ({'Picture': 'img', 'Video': 'vid'}.get(msg['Type'], 'fil'), msg['FileName'])
#
#
# # 现在微信加了好多群，并不想对所有的群都进行设置微信机器人，只针对想要设置的群进行微信机器人，可进行如下设置
# @itchat.msg_register(TEXT, isGroupChat=True)
# def group_text_reply(msg):
#     print(msg)
#     print("group_text_reply", msg)
#     # 当然如果只想针对@你的人才回复，可以设置if msg['isAt']:
#     item = group_id(u'监控测试群')  # 根据自己的需求设置
#     if msg['ToUserName'] == item:
#         itchat.send(u'%s' % tuling(msg['Text']), item)


def send_to_one_group(msg, gname):
    """

    :param msg:
    :param gname:
    :return:
    """
    try:
        print(gname)
        rooms = itchat.search_chatrooms(gname)
        print("sdsdsad")
        print(rooms[0])
        if rooms is not None:
            username = rooms[0]["UserName"]
            print(username)
            itchat.send(msg, toUserName=username)
            print("has send successfully")
            return True
        else:
            return False
    except Exception:
        msg = traceback.format_exc()
        logger.warning(msg)
        return False


def find_friend(nick_name):
    """

    :param nick_name:
    :return:
    """
    for friend in itchat.get_friends():
        if friend['NickName'] == nick_name:
            return friend


def send_to_one_person(msg, pname):
    """

    :param msg:
    :param pname:
    :return:
    """
    try:
        friend = find_friend(pname)
        username = friend['UserName']
        itchat.send(msg=msg, toUserName=username)
        return True
    except Exception:
        msg = traceback.format_exc()
        logger.warning(msg)
        return None


if __name__ == "__main__":
    send_to_one_person(u'hello world ---- ', u'监控小助手')
    send_to_one_group(u'hello world  ====', u'监控测试群')
    itchat.run()
