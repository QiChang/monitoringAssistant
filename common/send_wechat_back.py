# -*- coding:utf-8 -*-
from itchat.content import *
import requests
import json
import itchat
# from __future__ import unicode_literals
from wxpy import *
# import unicode
import requests
from common.send_email import send_notify_email
from threading import Timer

itchat = Bot(console_qr=2, cache_path="itchat.pkl")
# itchat.auto_login(hotReload=True)
# itchat.auto_login(hotReload=True)
# # 调用图灵机器人的api，采用爬虫的原理，根据聊天消息返回回复内容
# def tuling(info):
#     print("tuling",info)
#     print(type(info))
#     # if(info.endswith("天气怎么样")):
#     appkey = "e5ccc9c7c8834ec3b08940e290ff1559"
#     url = "http://www.tuling123.com/openapi/api?key=%s&info=%s"%(appkey,info)
#     req = requests.get(url)
#     content = req.text
#     data = json.loads(content)
#     answer = data['text']
#     return answer
#     # else:
#     #     return None
#
# # 对于群聊信息，定义获取想要针对某个群进行机器人回复的群ID函数
# def group_id(name):
#     print(name)
#     df = itchat.search_chatrooms(name=name)
#     return df[0]['UserName']
#
# # 注册文本消息，绑定到text_reply处理函数
# # text_reply msg_files可以处理好友之间的聊天回复
# @itchat.msg_register([TEXT,MAP,CARD,NOTE,SHARING])
# def text_reply(msg):
#     print("text_reply",text_reply)
#     itchat.send('%s' % tuling(msg['Text']),msg['FromUserName'])
#
# @itchat.msg_register([PICTURE, RECORDING, ATTACHMENT, VIDEO])
# def download_files(msg):
#     print("download_files",msg)
#     msg['Text'](msg['FileName'])
#     return '@%s@%s' % ({'Picture': 'img', 'Video': 'vid'}.get(msg['Type'], 'fil'), msg['FileName'])
#
# # 现在微信加了好多群，并不想对所有的群都进行设置微信机器人，只针对想要设置的群进行微信机器人，可进行如下设置
# @itchat.msg_register(TEXT, isGroupChat=True)
# def group_text_reply(msg):
#     print(msg)
#     print("group_text_reply", msg)
#     # 当然如果只想针对@你的人才回复，可以设置if msg['isAt']:
#     item = group_id(u'监控测试群')  # 根据自己的需求设置
#     if msg['ToUserName'] == item:
#         itchat.send(u'%s' % tuling(msg['Text']), item)


def getNews():
    url = "http://open.iciba.com/dsapi/"
    r = requests.get(url)
    content = r.json()['content']
    note = r.json()['note']
    print("___________")
    print(content)
    print(note)
    print("==========")
    return content, note


def send_to_one_person(**kwargs):
    message = str(
        "告警服务器:{HostName}\n"
        "告警时间:{TriggerTime}\n"
        "告警IP:{HostIP}\n"
        "告警项:{Description}\n"
        "告警级别:{level}\n"
        "告警信息:{msg}\n"
        "收件箱:{email}".format(
            HostName=kwargs["HostName"],
            TriggerTime=kwargs["TriggerTime"],
            HostIP=kwargs["HostIP"],
            Description=kwargs["Description"],
            level=kwargs["level"],
            msg=kwargs["msg"],
            email=kwargs['email']
        ))


    try:
        itchat = Bot(console_qr=2, cache_path="itchat.pkl")
        userName= kwargs["ToUserName"]
        friend = itchat.friends().search(name=userName)
        send_notify_email("测试",message1,receiver=kwargs['email'])
        print(message1)
        for index, item in enumerate(friend):
            print("发送给 " + str(item) + " ing,index=" + str(index))
            item.send(message1)
    except:
        errorMessage = "发送错误"
        for index, item in enumerate(friend):
            item.send(errorMessage)


# itchat.run()

if __name__ == "__main__":
    cao = dict()
    cao = {
        "HostName":"中央办公厅",
        "TriggerTime":"2018-10-10 00:00:00",
        "HostIP":"111.111.6666.111",
        "Description":"中奖11111110万",
        "level":"紧急",
        "msg":"赶紧领取",
        "ToUserName":"Dearest",
        "email":"yinqichang@laiye.com"
    }
    send_to_one_person(cao)
    # itchat.run()