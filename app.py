# -*- coding: UTF-8 -*-
import os
import sys
import tornado.web
import tornado.ioloop
import tornado.httpserver
from common import wulai_logger as logger
from common.wulai_constant import *

def load_handlers(name: str) -> list:

    """
    Load the (URL pattern, handler) tuples for each component.

    :param name: handlers dir
    :type name: str
    :return: URL pattern, handler
    :rtype: list
    """
    mod = __import__(name, fromlist=['default_handlers'])
    return mod.default_handlers


class Application(tornado.web.Application):
    def __init__(self):
        handlers = []

        handlers.extend(load_handlers('handlers.SendAlarm'))
        handlers.extend(load_handlers('handlers.BmiPicture'))
        handlers.extend(load_handlers('handlers.AppletFront'))
        handlers.extend(load_handlers('handlers.ApiIntroduction'))
        handlers.extend(load_handlers('handlers.TestMessageRoad'))
        # set the URL that will be redirected from `/`
        handlers.append(
            (r'/?', tornado.web.RedirectHandler, {
                'url': 'static/a/main.html'
            })
        )

        settings = dict(
            # static_path=os.path.join(os.path.dirname(os.path.abspath(__file__)), "static"),
            static_path=os.path.join(os.path.dirname(os.path.abspath(__file__)), "static"),
            template_path=os.path.join(os.path.dirname(__file__), "templates"),
            debug=True
        )
        logger.info(os.path.join(os.path.dirname(os.path.abspath(__file__)), "static"))
        tornado.web.Application.__init__(self, handlers=handlers, **settings)


if __name__ == "__main__":
    application = Application()
    http_server = tornado.httpserver.HTTPServer(application)
    logger.info(DEFAULT_PORT)
    port = DEFAULT_PORT
    http_server.listen(port)
    logger.info("Web run at local port: {}".format(port))
    tornado.ioloop.IOLoop.instance().start()

# Sept-12-2018  QiChang.Yin      created.