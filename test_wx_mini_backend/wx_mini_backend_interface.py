# -*- coding: utf-8 -*-

import requests
import json
import time
import os
from common.wulai_logger import logger
import datetime
import random
import inspect
import threading,time
from common.send_email import send_notify_email

def get_news():
    #获取一个连接中的内容
    url = "http://open.iciba.com/dsapi/"
    r = requests.get(url)
    content = r.json()['content']
    note = r.json()['note']
    return content, note

def test_callback():
    url = 'http://mplugin.wul.ai/callback'
    content = get_news()
    str = content[1]
    print(str)
    body = {
        "msg_id": "123123131",
        "msg_ts": 1537252907768,
        "user_id": "test123",
        "msg_body": {


            "text": {
                "content": str
            },
            "extra": ""},
        "user_request": [],
        "intent": [],
        "sender_info": {
            "real_name": "陆菲",
            "avatar_url": ""

        }
    }

    response = requests.post(url, data=json.dumps(body))
    # 返回信息
    print("-----")
    print (response.text)
    # 返回响应头
    print (response.status_code)
    # logger.error('Ron Yun Code: {0} : {1}'.format(response.status_code, str))
    print(response.status_code)
    return response.status_code

def get_open_wulai():
    url = 'https://mplugin.wul.ai/get_configure2?client=f8Hpkr5cJWOaegCLbQ6VD1ES8g9W8xL900cb60581e0ba93511'
    response = requests.get(url, timeout=5)
    # 返回信息
    print(response.text)
    # 返回响应头
    print(response.status_code)
    if response.status_code == "200":
        print("aaa")
    else:
        print("BBB")

def test_rongcloud():
    url = 'http://mplugin.wul.ai/rongcloud?timestamp=14087106534911&nonce=14314&signature=45beb7cc7307889a8e711219a47b7cf6a5b000e8'
    body = {
    "userid": "apert60592",
    "status": "1",
    "os": "Android",
    "time": "1437982625625"
    }

    try:
        response = requests.post(url, data=json.dumps(body),timeout=5)
        # 返回信息
        print(response.text)
        # 返回响应头
        print(response.status_code)
        logger.error('Web SDK Code: {} : {}'.format(response.status_code, response.text))
    except requests.exceptions.Timeout:
        print("Timeout occurred")
        send_notify_email("测试","{}Timeout occurred".format(inspect.stack()[1][3]))

    return response.status_code

if __name__ == "__main__":
    # threads = []
    print("AAA")
    # print(os.system("ping openapi.wul.ai"))


    # if result.
    # for i in range(180):
    #     t = threading.Thread(target=test_web_sdk, args=())  # target=show执行show函数，后面args表示show函数中传入的参数
    #     threads.append(t)
    #     # t.start()
    while True:
        # test_callback()
        # time.sleep(3)
        # test_rongcloud()
        time.sleep(1)
        get_open_wulai()

    # # t1.join()    #停止主线程，直到执行完子线程t1后才执行主线程
    # t.join()    #停止主线程，直到执行完子线程t2后才执行主线程
    # print('all is over...%s' % time.ctime())
    # for t in threads:
    #     t.setDaemon(True)
    #     t.start()
    # # for t in threads:
    # #     t.join()
    # print("all over")