
msg_title = ["【🚔🚔🚔渠道后端报警🚔🚔🚔】",
             "【🔔🔔🔔程序前端报警🔔🔔🔔】",
             "【🚑🚑🚑消息路由报警🚑🚑🚑】",
             "【🚀🚀🚀吾来论语报警🚀🚀🚀】",
             "【🌵🌵🌵版本发布通知🌵🌵🌵】",
             "【🍀🍀🍀测试报告通知🍀🍀🍀】"
             ]

msg_key = {

   # 公共部分
   # 0，1，2，3，4对应msg_title中的标题
   "target_type": "目标类型",
   "to_user_name": "接收用户",
   "from_user_name": "发送用户",
   "to_email": "接收邮箱",
   "to_email_title": "邮件主题",
   "to_group_name": "接收群名",

   #  报警信息通知
   "alarm_host": "主机名称",
   "alarm_ip":   "主机    IP",
   "alarm_position": "报警位置",
   "alarm_project": "报警项目",
   "alarm_log": "详细日志",
   "alarm_environment": "报警环境",
   "alarm_time": "报警时间",
   "alarm_level": "报警级别",
   "alarm_content": "报警内容",
   "alarm_remark": "备注信息",

   # 版本发布通知
   "git_tag": "版本标签",
   "git_message": "版本信息",
   "git_trigger": "发布用户",
   "git_url": "发布地址",
   "git_time": "发布时间",
   "git_project": "发布项目",
   "git_remark": "备注信息",

   # 测试报告通知
   "test_result": "测试结果",
   "test_content": "测试内容",
   "test_project": "测试项目",
   "test_time": "测试时间",
   "test_report": "测试报告",
   "test_person": "测试总监",
   "test_remark": "备注信息"
}

if __name__ == "__main__":
     msg = ""
     dic = {
         "to_group_name":"qqq",
         "content":"eee",
         "level":"sasdads",
         "dateTime":"dateTime",
         "to_user_name": "to_user_name"
     }
     get_value = {v: dic.get(k) for k, v in msg_key.items() if k in dic}
     for k in sorted(get_value.keys(), reverse=True):
         msg = msg + (k+":"+get_value[k]+'\n').format(get_value[k])
     print(msg)
