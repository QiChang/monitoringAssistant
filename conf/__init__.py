# -*- coding: UTF-8 -*-
# -*- coding: UTF-8 -*-
import os
project_env = os.environ.get("PROJECT")
if project_env == 'prod':
    from .prod import *
else:
    from .dev import *
